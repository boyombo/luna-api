KEY = 'c6t95zbb554at'
SECRET = 'YhI8DECqy199TGKOpB9tqpUgYc5pFw68nZsZptI9jV0'
from pybitx.api import BitX
from operator import itemgetter
import time


class BitXDriver(object):
    key = KEY
    secret = SECRET
    limit_btc = 0.001
    BUY = 0
    SELL = 1

    def __init__(self):
        options = {
            'pair': 'XBTNGN'
        }
        self.api = BitX(self.key, self.secret, options)
        self.next = self.BUY
        self.ngn_value = 500.0
        self.btc_value = 0.0

    def get_ticker(self):
        return self.api.get_ticker()

    def _get_trades(self):
        return self.api.get_trades(limit=9)

    def show_values(self):
        print 'btc {}, ngn {}'.format(self.btc_value, self.ngn_value)

    def _buy(self, amt):
        ticker = self.get_ticker()
        ask = float(ticker['ask'])
        print ask
        self.btc_value = amt/ask
        self.ngn_value = 0.0
        #self.show_values()
        print 'bought btc'

    def _sell(self, amt):
        ticker = self.get_ticker()
        bid = float(ticker['bid'])
        print bid
        self.ngn_value = bid * amt
        self.btc_value = 0.0
        #self.show_values()
        print 'sold btc'

    def decision(self):
        trades = self._get_trades()['trades']
        trades.sort(key=itemgetter('timestamp'))
        top = sum(int(float(i['price'])) for i in trades[-3:])
        middle = sum(int(float(i['price'])) for i in trades[3:6])
        bottom = sum(int(float(i['price'])) for i in trades[:3])
        print 'top: {}, middle: {}, bottom: {}'.format(top, middle, bottom)
        #print trades

        if self.next == self.BUY:
            # you buy when prices are rising
            if middle < top and middle < bottom:
                # buy with available cash
                self._buy(self.ngn_value)
                self.next = self.SELL
            else:
                print 'not favourable to buy'
        else:
            # sell when prices are falling
            if middle > top and middle > bottom:
                self._sell(self.btc_value)
                self.next = self.BUY
            else:
                print 'not favourable to sell'
        self.show_values()

if __name__ == '__main__':
    bx = BitXDriver()
    for i in range(180):
        bx.decision()
        time.sleep(10)
