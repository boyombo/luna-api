from main import BitXDriver
import json
import pandas
import time

PERIOD = 60  # 60 seconds
SIZE = 26  # size of the history
MACD_SLOW = 26
MACD_FAST = 12
MACD_SIGNAL = 9

data = []


class Macd(BitXDriver):
    def __init__(self):
        super(Macd, self).__init__()
        self.history = []
        self.macd = []
        self.signal = []

    def refresh(self):
        last_trade = float(self.get_ticker()['last_trade'])
        self.history.append(last_trade)
        if len(self.history) > SIZE:
            self.history.pop(0)
        print "History"
        print self.history
        print "MACD"
        print self.macd
        print "Signal"
        print self.signal
        print "------------------------------"

    def weighted_avg(self, size, values):
        weight = pandas.np.exp(pandas.np.linspace(-1., 0., size))
        weight /= weight.sum()
        return sum(i[0]*i[1] for i in zip(values, weight))

    def cal_macd(self):
        avg_slow = self.weighted_avg(MACD_SLOW, self.history)
        avg_fast = self.weighted_avg(MACD_FAST, self.history[-MACD_FAST:])
        _macd = avg_fast - avg_slow
        self.macd.append(_macd)
        # signal exponential avg
        size = len(self.macd) if len(self.macd) < MACD_SIGNAL else MACD_SIGNAL
        avg_signal = self.weighted_avg(size, self.macd[-size:])
        self.signal.append(avg_signal)

    def should_buy(self):
        if self.macd[-1] > 0 and self.signal[-1] > 0:
            if self.signal[-1] >= self.macd[-1]:
                return True
        return False

    def should_sell(self):
        if self.macd[-1] < 0 or self.signal[-1] < 0 or self.signal[-1] < self.macd[-1]:
            return True
        return False

    def decision(self):
        self.refresh()
        if len(self.history) < SIZE:
            return

        self.cal_macd()
        if self.next == self.BUY and self.should_buy():
            self._buy(self.ngn_value)
            self.next = self.SELL
            self.persist()
        elif self.next == self.SELL and self.should_sell():
            self._sell(self.btc_value)
            self.next = self.BUY
            self.persist()

    def persist(self):
        d = {
            'history': self.history,
            'macd': self.macd,
            'signal': self.signal
        }
        f = open('data.json', 'w')
        json.dump(d, f)
        f.close()

if __name__ == '__main__':
    bx = Macd()
    for i in range(180):
        bx.decision()
        time.sleep(PERIOD)
